package ch.hotmilk.tares.client.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import ch.hotmilk.embrava.core.BlynclightColors;
import ch.hotmilk.embrava.core.BlynclightLightControl;
import ch.hotmilk.embrava.core.BlynclightRing;
import ch.hotmilk.embrava.core.IEmbrava;

@SpringBootTest
class BlynclightTestingControllerTest {

	@MockBean
	IEmbrava embrava;

	@Autowired
	BlynclightTestingController controller;

//	@Test
//	public void applicationNameTest() throws Exception {
//		assertThat(controller).isNotNull();
//		assertThat(controller.getApplicationName()).isEqualTo("Blynclight");
//	}

	@Test
	public void testingBlynclightEmptyDeviceTest() {
		when(embrava.getAllDeviceIds()).thenReturn(Collections.emptyList());
		assertThat(controller.testingBlynclight()).isEqualTo("No device connected");
	}

	@Test
	public void testingBlynclighWithDevices() throws Exception {

		List<String> deviceIds = new ArrayList<String>();
		deviceIds.add("One");
		deviceIds.add("Two");
		when(embrava.getAllDeviceIds()).thenReturn(deviceIds);

		when(embrava.sendToDevice(BlynclightColors.BLUE, BlynclightLightControl.FAST, BlynclightRing.SOUND_OFF, 2))
				.thenReturn(1);

		assertThat(controller.testingBlynclight()).isEqualTo("test is running");
	}

	@Test
	public void testingBlynclighMinusDeviceId() throws Exception {
		List<String> deviceIds = new ArrayList<String>();
		deviceIds.add("One");
		deviceIds.add("Two");
		when(embrava.getAllDeviceIds()).thenReturn(deviceIds);

		when(embrava.sendToDevice(BlynclightColors.BLUE, BlynclightLightControl.FAST, BlynclightRing.SOUND_OFF, 2))
				.thenReturn(-1);

		assertThat(controller.testingBlynclight())
				.isEqualTo("Some error popped up during the testing. Please try again later");
	}
}
