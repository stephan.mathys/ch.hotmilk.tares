package ch.hotmilk.tares.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.hotmilk.embrava.core.BlynclightColors;
import ch.hotmilk.embrava.core.BlynclightEvent;
import ch.hotmilk.embrava.core.BlynclightLightControl;
import ch.hotmilk.embrava.core.BlynclightRing;
import ch.hotmilk.embrava.core.IBlynclightListener;
import ch.hotmilk.embrava.core.IEmbrava;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class BlynclightTestingController implements IBlynclightListener {

	@Autowired
	private IEmbrava embara;

	@GetMapping("/testingBlynclight")
	public @ResponseBody String testingBlynclight() {

		// TODO get the deviceId first
		embara.sendToDevice(BlynclightColors.RED, BlynclightLightControl.SLOW, BlynclightRing.SOUND_OFF, 3);

		return "finish";
	}

	@Override
	public void removed(BlynclightEvent event) {
		log.debug("Blynclight added");
	}

	@Override
	public void attached(BlynclightEvent event) {
		log.debug("Blynclight removed");
	}

	@GetMapping("/scan/start")
	public String startScanning() {
		embara.startScanning();
		return "Scanning started";
	}
}
