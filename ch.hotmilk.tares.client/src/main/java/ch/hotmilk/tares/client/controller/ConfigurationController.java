package ch.hotmilk.tares.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class ConfigurationController {

	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${tares.table.name:no-table-name}")
	private String tableName;

	@GetMapping("/config/app-name")
	public String getApplicationName() {
		log.debug("Application name: {1}", applicationName);
		return applicationName;
	}

	@GetMapping("/config/table-name")
	public String getTableName() {
		log.debug("Table name: {1}", tableName);
		return tableName;
	}

}
