package ch.hotmilk.tares.client.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.hotmilk.embrava.core.BlynclightColors;
import ch.hotmilk.embrava.core.BlynclightLightControl;
import ch.hotmilk.embrava.core.BlynclightRing;
import ch.hotmilk.embrava.core.IEmbrava;

@Service
public class BlynclightService {

	@Autowired
	private IEmbrava embara;

	public List<BlynclightColors> getAllColors() {
		ArrayList<BlynclightColors> lst = new ArrayList<BlynclightColors>();
		Collections.addAll(lst, BlynclightColors.values());
		return lst;
	}

	public List<BlynclightLightControl> getAllControls() {
		ArrayList<BlynclightLightControl> list = new ArrayList<BlynclightLightControl>();
		Collections.addAll(list, BlynclightLightControl.values());
		return list;
	}

	public List<BlynclightRing> getAllRingTones() {
		ArrayList<BlynclightRing> list = new ArrayList<BlynclightRing>();
		Collections.addAll(list, BlynclightRing.values());
		return list;
	}

	public void BlincLight(String color, Integer howManySeconds) {
		embara.sendToDevice(BlynclightColors.findColor(color), BlynclightLightControl.SLOW, BlynclightRing.SOUND_OFF,
				howManySeconds);
	}
}
