package ch.hotmilk.tares.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.hotmilk.tares.client.service.BlynclightService;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class LightController {

	@Autowired
	BlynclightService blinclightService;

	@GetMapping("/light/blinc")
	public String blincLight(@RequestParam(defaultValue = "green") String color) {
		blinclightService.BlincLight(color, 5);
		return null;
	}
}
