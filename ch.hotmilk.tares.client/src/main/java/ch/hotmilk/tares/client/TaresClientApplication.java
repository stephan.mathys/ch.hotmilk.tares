package ch.hotmilk.tares.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import ch.hotmilk.embrava.core.IEmbrava;
import ch.hotmilk.embrava.core.services.EmbravaService;
import lombok.extern.log4j.Log4j2;

@EnableEurekaClient
@SpringBootApplication
@Log4j2
public class TaresClientApplication {
	public static void main(String[] args) {
		log.debug("Starting tares client");
		SpringApplication.run(TaresClientApplication.class, args);
	}

	@Bean
	public IEmbrava createEmbravaService() {
		log.debug("create Embrava Bean");
		return new EmbravaService();
	}
}
