package ch.hotmilk.tares.server.controller.service;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.shared.Application;
import com.netflix.eureka.EurekaServerContextHolder;
import com.netflix.eureka.registry.PeerAwareInstanceRegistry;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class ClientDiscoveryService {

	private String getClientApplicationUrl(String blynclightId) {
		PeerAwareInstanceRegistry registry = EurekaServerContextHolder.getInstance().getServerContext().getRegistry();
		List<Application> sortedApplications = registry.getSortedApplications();
		for (Application app : sortedApplications) {
			if (app.getName().equalsIgnoreCase(blynclightId)) {
				List<InstanceInfo> instances = app.getInstances();
				for (InstanceInfo instance : instances) {
					log.info("found Url " + instance.getHomePageUrl());
					return instance.getHomePageUrl();
				}
			}
		}
		log.debug("application " + blynclightId + " not found");
		return "application " + blynclightId + " not found";
	}

	private String callService(String fooResourceUrl) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<>(new String());
		ResponseEntity<String> response = null;
		try {
			log.debug("Call url " + fooResourceUrl);
			response = restTemplate.exchange(fooResourceUrl, HttpMethod.GET, request, String.class);
			log.debug("Headers for the Response: " + response.getHeaders().toString());
			log.debug("Body for the Response: " + response.hasBody());
			return response.getBody();

		} catch (Exception e) {
			throw new RuntimeException("some error by call to " + fooResourceUrl, e);
		}
	}

	public String callClientToBlink(String table) {
		String baseUrl = getClientApplicationUrl(table);
		return callService(baseUrl + "light/blinc");
	}

	public String getUrl(String appName) {
		return getClientApplicationUrl(appName);
	}
}
