package ch.hotmilk.tares.server.controller.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.hotmilk.tares.server.controller.service.ClientDiscoveryService;
import ch.hotmilk.tares.server.controller.service.ReservationService;
import ch.hotmilk.tares.server.model.Reservation;
import ch.hotmilk.tares.server.model.TableReservation;
import ch.hotmilk.tares.server.model.User;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
public class ReservationController {

	@Autowired
	private ReservationService reservationService;

	@Autowired
	ClientDiscoveryService discoveryService;

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	@GetMapping("/reservation/add")
	public String addReservation(@RequestParam String startingTime, @RequestParam String endingTime,
			@RequestParam String tablename, @RequestParam String username) {

		LocalDateTime dateStartingTime = LocalDateTime.parse(startingTime, formatter);
		LocalDateTime dateEndingTime = LocalDateTime.parse(endingTime, formatter);

		User user = reservationService.findByUserName(username);
		TableReservation table = reservationService.findByTablename(tablename);
		Reservation reservation = new Reservation(dateStartingTime, dateEndingTime, table, user);
		reservationService.addReservation(reservation);
		log.debug("reservation added");
		return "ok";
	}

	@GetMapping("/reservation/search/between")
	public String findTableBetween(@RequestParam String startingTime, @RequestParam String endingTime) {

		LocalDateTime dateStartingTime = LocalDateTime.parse(startingTime, formatter);
		LocalDateTime dateEndingTime = LocalDateTime.parse(endingTime, formatter);

		List<TableReservation> freeTables = reservationService.searchFreeTable(dateStartingTime, dateEndingTime);
		ObjectMapper mapper = new ObjectMapper();
		// TODO spring-boot-starter-json

		String jString = null;
		try {
			jString = mapper.writeValueAsString(freeTables);
		} catch (IOException e) {
			log.error("could not convert tables to jason", e);
		}

		return jString;
	}

	@GetMapping("/table/show-free")
	public String freeTablesToBlink() {
		LocalDateTime now = LocalDateTime.now();
		List<TableReservation> searchFreeTable = reservationService.searchFreeTable(now, now.plusHours(4));
		for (TableReservation tableReservation : searchFreeTable) {

			discoveryService.callClientToBlink(tableReservation.getTableName());
		}
		return "tables blinked";
	}

}
