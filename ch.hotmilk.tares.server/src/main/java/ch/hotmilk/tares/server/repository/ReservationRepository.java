package ch.hotmilk.tares.server.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ch.hotmilk.tares.server.model.Reservation;
import ch.hotmilk.tares.server.model.TableReservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

//	Select t from TABLE_RESERVATION t where id NOT IN (Select table_id from RESERVATIONS where  (STARTING_DATE >= :startingTime and ENDING_DATE <=  :endingTime))
	@Query("Select t from TableReservation t where id NOT IN (Select table from Reservation where  (startingDate >= :startingDate and endingDate <= :endingDate))")
	List<TableReservation> findFreeTableBetween(@Param("startingDate") LocalDateTime startingDate,
			@Param("endingDate") LocalDateTime endingDate);

}
