package ch.hotmilk.tares.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.hotmilk.tares.server.model.TableReservation;

public interface TableReservationRepository extends JpaRepository<TableReservation, Long> {

	TableReservation findByTableName(String tablename);

}
