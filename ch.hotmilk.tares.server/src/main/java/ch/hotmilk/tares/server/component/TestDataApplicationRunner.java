package ch.hotmilk.tares.server.component;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TestDataApplicationRunner implements ApplicationRunner {

	@PersistenceContext
	private EntityManager entityManager;

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	@Transactional
	@Override
	public void run(ApplicationArguments args) throws Exception {
//		String insertReservation =  "INSERT INTO reservations (starting_date, ending_date, table_id, user_id) VALUES ('2020-10-01 08:00:00', '2020-10-01 17:00:00', 1, 1)";
		String insertReservation = "INSERT INTO reservations (starting_date, ending_date, table_id, user_id) VALUES (?, ?, ?, ?)";

		LocalDateTime now = LocalDateTime.now();
		LocalDateTime todayMidnight = now.with(LocalTime.MIDNIGHT);

		Random r = new Random();
		int reservationStartingTimeMorning = 8;
		int breservationStartingTimeAfternoon = 13;

		// do it for each table
		for (int table = 1; table < 6; table++) {

			// do it for the next 10 days
			for (int reservationDay = 0; reservationDay < 10; reservationDay++) {
				int reservationStartingTime = r.nextBoolean() ? reservationStartingTimeMorning
						: breservationStartingTimeAfternoon;
				LocalDateTime startingTime = todayMidnight.plusDays(reservationDay).plusHours(reservationStartingTime);

				entityManager.createNativeQuery(insertReservation).setParameter(1, startingTime.format(formatter))
						.setParameter(2, startingTime.plusHours(4).format(formatter)).setParameter(3, table)
						.setParameter(4, 1).executeUpdate();
			}
		}

	}

}
