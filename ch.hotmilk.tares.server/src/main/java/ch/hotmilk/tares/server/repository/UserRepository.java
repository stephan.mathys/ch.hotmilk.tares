package ch.hotmilk.tares.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.hotmilk.tares.server.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserName(String user);

}
