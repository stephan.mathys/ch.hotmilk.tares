package ch.hotmilk.tares.server.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "reservations")
@Data
public class Reservation {

	public Reservation(LocalDateTime startingTime, LocalDateTime endingTime, TableReservation table, User user) {
		this.startingDate = startingTime;
		this.endingDate = endingTime;
		this.user = user;
		this.table = table;

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private LocalDateTime startingDate;
	private LocalDateTime endingDate;
	@ManyToOne
	private User user;
	@ManyToOne
	private TableReservation table;

}
