package ch.hotmilk.tares.server.controller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import ch.hotmilk.tares.server.controller.service.ClientDiscoveryService;
import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Mono;

@RestController
@Log4j2
public class ClientApplicationController {

	@Autowired
	ClientDiscoveryService discoveryService;

	@Autowired
	WebClient.Builder webClientBuilder;

	@GetMapping("/clients/show")
	public String getClients(@RequestParam(name = "blynclightId") String blynclightId) {
		return discoveryService.getUrl(blynclightId);
	}

	@GetMapping("/testingClient")
	public String testBlink(@RequestParam(name = "blynclightId") String blynclightId) {
		String clientUrl = discoveryService.getUrl(blynclightId);

		WebClient webClient = webClientBuilder.baseUrl(clientUrl).build();
		Mono<String> bodyToMono = webClient.get().uri("/testingBlynclight").retrieve().bodyToMono(String.class);
		log.debug(bodyToMono.block());
		return "exchange";
	}
}
