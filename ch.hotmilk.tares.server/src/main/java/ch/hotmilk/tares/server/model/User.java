package ch.hotmilk.tares.server.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "user_reservation")
@Data
public class User {

	@Id
	public Long id;
	@Column(name = "user_name")
	public String userName;
	@OneToMany(targetEntity = Reservation.class)
	List<Reservation> reservations;
}
