package ch.hotmilk.tares.server.controller.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.hotmilk.tares.server.model.Reservation;
import ch.hotmilk.tares.server.model.TableReservation;
import ch.hotmilk.tares.server.model.User;
import ch.hotmilk.tares.server.repository.ReservationRepository;
import ch.hotmilk.tares.server.repository.TableReservationRepository;
import ch.hotmilk.tares.server.repository.UserRepository;

@Service
public class ReservationService {

	@Autowired
	UserRepository userRepo;
	@Autowired
	TableReservationRepository tableRepo;
	@Autowired
	ReservationRepository reservationRepo;

	public User findByUserName(String username) {
		return userRepo.findByUserName(username);
	}

	public TableReservation findByTablename(String tablename) {
		return tableRepo.findByTableName(tablename);

	}

	public void addReservation(Reservation reservation) {
		reservationRepo.save(reservation);
	}

	public List<TableReservation> searchFreeTable(LocalDateTime startingTime, LocalDateTime endingTime) {

		return reservationRepo.findFreeTableBetween(startingTime, endingTime);

	}
}
