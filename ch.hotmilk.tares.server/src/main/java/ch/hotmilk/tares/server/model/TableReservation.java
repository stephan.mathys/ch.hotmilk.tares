package ch.hotmilk.tares.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "table_reservation")
@Data
public class TableReservation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	@Column(name = "table_name")
	public String tableName;
//	@OneToMany(targetEntity = Reservation.class)
//	List<Reservation> reservations;

}
