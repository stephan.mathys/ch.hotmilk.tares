DROP TABLE IF EXISTS TABLE_RESERVATION_RESERVATIONS;
DROP TABLE IF EXISTS USER_RESERVATION_RESERVATIONS;

DROP TABLE IF EXISTS user_reservation;
CREATE TABLE user_reservation (
  id int NOT NULL AUTO_INCREMENT,
  user_name VARCHAR(250),
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS table_reservation;
CREATE TABLE table_reservation (
  id int NOT NULL AUTO_INCREMENT,
  table_name VARCHAR(250),
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS reservations;
CREATE TABLE reservations (
  id int NOT NULL AUTO_INCREMENT,
  starting_date DATETIME,
  ending_date DATETIME,
  user_id int NOT NULL,
  table_id int NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_reservations_user
  	FOREIGN KEY (user_id) REFERENCES user_reservation (id),
  CONSTRAINT fk_reservations_table_reservation
  	FOREIGN KEY (table_id) REFERENCES table_reservation (id)
);
